//import 'package:firebase/profile.dart';
import 'dart:io';
import 'dart:async';
import 'package:firebase/forward.dart';
import 'package:firebase/group.dart';
import 'package:firebase/profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

final ThemeData iOSTheme = new ThemeData(
  primarySwatch: Colors.red,
  primaryColor: Colors.grey[400],
  primaryColorBrightness: Brightness.dark,
);

final ThemeData androidTheme = new ThemeData(
  primarySwatch: Colors.blue,
  accentColor: Colors.green,
);

String defaultUserName;

class messageGroup extends StatefulWidget {
  final String value1;
  final String value2;

  messageGroup({Key key, this.value1, this.value2,}) : super(key: key);

  @override
  _messageHereState createState() => _messageHereState();
}
class _messageHereState extends State<messageGroup> {
  String user_Name;
  String userId;
  String isMe;
  String you;
  String meANDyou;
  String me;
  int test = 40;
  DatabaseReference databaseId;
  final TextEditingController _textController = new TextEditingController();
  bool _isWriting = false;

  bool change = false;
  String delete = null;
  String forward = null;
  File Image1 = null;



  List<Item> UsersList = List();
  Item item;

  @override
  void initState() {
    super.initState();
    isMe = "${widget.value2}";
    databaseId = FirebaseDatabase.instance.reference().child("GroupMessage").child("${widget.value1}").child("Messages");
  }

  @override
  Widget build(BuildContext ctx) {
    //final bg = Colors.greenAccent.shade100;
    final bg = Colors.deepPurpleAccent;
    //final icon = send1 ? Icons.done_all : Icons.done;
    //final bg = isMe ? Colors.white : Colors.greenAccent.shade100;

    return new Scaffold(
        appBar:change == false ? new AppBar(
          title: Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8, 0, 0),
                child: CircleAvatar(
//                    radius: 20.0,
//                    backgroundColor: Colors.transparent,
//                    backgroundImage: AssetImage("assets/one.jpg")
                ),         // new Image.network("http://res.cloudinary.com/kennyy/image/upload/v1531317427/avatar_z1rc6f.png"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0, 0),
                child: Column(children: [

                  Text("${widget.value1}"),
                  GestureDetector(
                    child: Text('members',style: TextStyle(fontSize: 12),),
                    onTap: () {
                      Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => new profile(group :"${widget.value1}"),
                            ),
                          );
                    },
                  )
                ]),
              ),

        // action button

            ],
          ),
            actions: <Widget>[
        // action button
        IconButton(
        icon: Icon(Icons.person_add),
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(

            builder: (BuildContext context) => new Group(grpName: "${widget.value2}", isMe: "${widget.value2}"),
          ),
        );
      },
    ),],
          /*  leading: new Padding(
            padding: const EdgeInsets.all(8.0),
            child: CircleAvatar(
                radius: 100.0,
                backgroundColor: Colors.transparent,
                backgroundImage: AssetImage("assets/one.jpg")
            ),         // new Image.network("http://res.cloudinary.com/kennyy/image/upload/v1531317427/avatar_z1rc6f.png"),
          ),*/

          //title: new Text("${widget.value1}"),
          backgroundColor: Colors.deepPurpleAccent,
          elevation:
          Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
        )
            :new AppBar(

          title: new Text("change"),
          backgroundColor: Colors.deepPurpleAccent,
          elevation:
          Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                databaseId.child(delete).remove();
                setState(() {
                  change =false;
                });
              },
            ),
            // action button
            IconButton(
              padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
              icon: Icon(Icons.near_me),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => new Forward(isMe :"${widget.value2}", tomsg: "${widget.value1}", message : forward),
                  ),
                );
                //_select(choices[1]);
              },
            ),
            // overflow menu

          ],
        ),

        body:  new Container(
          child: new Column(
            //crossAxisAlignment: CrossAxisAlignment.stretch,

            children: <Widget>[
              new Text("ONE TO MANY",style: TextStyle(fontSize: 20.0),),

              Flexible(
                child: FirebaseAnimatedList(
                  sort: (DataSnapshot a, DataSnapshot b) =>
                      b.key.compareTo(a.key),
                  query: databaseId,

                  reverse: true,
                  itemBuilder: (context, snapshot, animation, index) {
                    return  Padding(
                      //******* separate sender and receiver  messages*******\\
                      padding: isMe == snapshot.value['sender']? const EdgeInsets.fromLTRB(25.0, 10.0, 150.0, 0.0)
                          :const EdgeInsets.fromLTRB(150.0, 10.0, 25.0, 0.0),
                      child: Column(
                        crossAxisAlignment: isMe == snapshot.value['sender']? CrossAxisAlignment.start: CrossAxisAlignment.end,

                        children: <Widget>[
                          new Text(snapshot.value['sender'],//textAlign: TextAlign.left,
                              style: new TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w900)),
                          new Container(

                            margin: const EdgeInsets.all(3.0),
                            padding: const EdgeInsets.all(8.0),
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: .5,
                                    spreadRadius: 1.0,
                                    color: Colors.black.withOpacity(.12))
                                //color: Colors.indigo.withOpacity(.12))

                              ],
                              color:isMe == snapshot.value['sender']? Colors.white : Colors.deepPurpleAccent, //********
                              //borderRadius: new BorderRadius.circular(5.0),

                              borderRadius: isMe == snapshot.value['sender']? new BorderRadius.only(
                                topRight: Radius.circular(10.0),
                                bottomLeft: Radius.circular(20.0),
                                bottomRight: Radius.circular(10.0),
                              ): BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                bottomLeft: Radius.circular(10.0),
                                bottomRight: Radius.circular(20.0),
                              ),
                            ),

                            child:
                            GestureDetector(
                              child: new Text(snapshot.value['message'],textAlign: TextAlign.center,
                                  style:
                                  new TextStyle(
                                      color: isMe == snapshot.value['sender']? Colors.deepPurpleAccent : Colors.white,
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w900)),
                              onLongPress: () {
                                setState(() {
                                  //mycolor1=Colors.red;
                                  final snackBar = SnackBar(content: Text(snapshot.value['message']+ " is selected",style: TextStyle(color: Colors.red),));
                                  Scaffold.of(context).showSnackBar(snackBar);

                                  //mycolor1=Colors.red;
                                  // mycolor= isMe == snapshot.value['sender']? Colors.red : Colors.red;
                                  change = true;
                                  delete = snapshot.key;
                                  forward = snapshot.value['message'];
                                });

                                print(snapshot.value['message']);
                              },
                            ),

                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),

              // new Text("save ${widget.value2}"),
              new Divider(height: 1.0),

              new Container(
                child: _buildComposer(),

                decoration: new BoxDecoration(color: Theme.of(ctx).cardColor),

              ),
            ],
          ),
          decoration: Theme.of(context).platform == TargetPlatform.iOS
              ? new BoxDecoration(
              border: new Border(
                  top: new BorderSide(
                    color: Colors.grey[200],
                  )))
              : null,
        )
    );
  }


  Widget _buildComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 9.0),
        child: new Row(

          children: <Widget>[

            new Container(
              margin: new EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
              child: new IconButton(
                  padding: new EdgeInsets.fromLTRB(0.0, 0, 2.0, 0),
                  icon: new Icon(Icons.camera, size: 30.0,),
                  onPressed: (){
                    print("test");
                    picker();
                  }// => _handleSubmitted(_textController.text)
              ),
            ),
            new Flexible(
              child: new TextField(
                maxLines: null,
                keyboardType: TextInputType.multiline,
                controller: _textController,
                onChanged: (String txt) {
                  setState(() {
                    _isWriting = txt.length > 0;
                  });
                },
                onSubmitted: _submitMsg,
                decoration: InputDecoration(
                  /*border: OutlineInputBorder(
                           borderRadius: BorderRadius.circular(32.0)
                       )*/
                  //contentPadding: new EdgeInsets.symmetric(vertical: 0.0),
                  // border: InputBorder.none,
                  /* prefixIcon: Padding(
                         padding: EdgeInsets.all(0.0),
                         child: Icon(
                           Icons.camera,
                           color: Colors.grey,
                             //onPressed:(){},
                         ),
                         // icon is 48px widget.
                       ),*/
                  /*suffixIcon:Padding(
                   padding: EdgeInsets.all(0.0),
                   child: Icon(
                     Icons.attachment,
                     color: Colors.grey,
                   ), // icon is 48px widget.
                 ),*/
                    border: InputBorder.none,
                    hintText: "Write a Message"),
              ),

            ),
            new Container(
              // margin: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0),
              child: new IconButton(
                  padding: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                  icon: new Icon(Icons.attachment, size: 30.0,),
                  alignment: FractionalOffset.centerLeft,
                  onPressed: (){
                    picker1();
                  }// => _handleSubmitted(_textController.text)
              ),
            ),


            new FloatingActionButton(
              //  elevation: 0.0,

                child: new Icon(Icons.near_me),

                backgroundColor: Colors.deepPurpleAccent,

                onPressed: (){_submitMsg(_textController.text);}
              /*_isWriting
                       ? () => _submitMsg(_textController.text)
                       : null,*/
            ),
            /*  new Container(
                   margin: new EdgeInsets.symmetric(horizontal: 3.0),
                   child: Theme.of(context).platform == TargetPlatform.iOS
                       ? new CupertinoButton(
                       child: new Text("Submit"),
                       onPressed: _isWriting ? () => _submitMsg(_textController.text)
                           : null
                   )
                       : new IconButton(
                     icon: new Icon(Icons.send),
                     onPressed: _isWriting
                         ? () => _submitMsg(_textController.text)
                         : null,
                   )
               ),*/
          ],
        ),
        decoration: Theme.of(context).platform == TargetPlatform.iOS
            ? new BoxDecoration(
            border:
            new Border(top: new BorderSide(color: Colors.brown))):null,
      ),
    );
  }

  void _submitMsg(String text) {
    print("working");
    //  uName();
    _sendMessage(sender:isMe, messageText: text, imageUrl: null);

    _textController.clear();
    setState(() {
      _isWriting = false;
    });
  }

  void _sendMessage({ String sender,String messageText, String imageUrl}) {
    print(" sender $sender");
    databaseId.push().set({
      'message': messageText,
      'sender' : sender
    });
  }

  void picker() async {
    print("camera");
    File img = await ImagePicker.pickImage(source: ImageSource.camera) as File;
    print(img.path);
    File Image = img;
    //_submitMsg(img);
    if (img != null) {

      setState(() {
        // _sendMessage(sender:isMe, messageText: img, imageUrl: null);

      });
    }
  }
  void picker1() async {
    print("camera");
    File img = await ImagePicker.pickImage(source: ImageSource.gallery) as File;
    print(img.path);
    //File Image1 = img;
    if (img != null) {

      setState(() {
        Image1=img;
      });
     // _showDialog();
    }
  }

}

class Item {
  String key;
  String sender;
  String message;

  Item(this.sender, this.message);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        sender = snapshot.value["sender"],
        message = snapshot.value["message"];

  toJson() {
    return {
      "sender": sender,
      "message": message,
    };
  }
}

