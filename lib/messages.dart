import 'package:firebase/contact.dart';
import 'package:firebase/group.dart';
import 'package:firebase/home_page.dart';
import 'package:firebase/msg.dart';
import 'package:firebase/msgGroup.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import "package:flutter_slidable/flutter_slidable.dart";


class Messages extends StatefulWidget {
  final String value1;
  Messages( {Key key, this.value1}) : super(key: key);


  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Messages> {
  final GrpController = TextEditingController();


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String userId;
  String OwnerName ;
  String Gp;
  String you;
  bool selected = false;

  var SelectedState = false;
  var mycolor=Colors.white;
  var isSelected = false;

  List<Item> msgList =new  List();
  Item item;
  DatabaseReference itemRef;
  DatabaseReference itemRefM;
  DatabaseReference itemRefMType;
  DatabaseReference databaseId;


  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  //TextEditingController controller = new TextEditingController();
  // TextEditingController controller2 = new TextEditingController();


  @override
  void initState() {
    super.initState();
    String OwnerName = "${widget.value1}";
    _init();
    print("ok");
    print("kkkkkkkkkkkkkk  you = $OwnerName");
    itemRefM = FirebaseDatabase.instance.reference().child('messagalu').child('$OwnerName');

    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance;
    //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    itemRef.onChildAdded.listen(_onEntryAdded);

  }

  Future _init() async {
    await FirebaseDatabase.instance.setPersistenceEnabled(true);

    /* itemRefMType=FirebaseDatabase.instance.reference().child("messagalu").child("${widget.value1}").child("$toMessage");
    itemRefMType.once().then((DataSnapshot snapshot){
      Map<dynamic, dynamic> values=snapshot.value;
      values.forEach((key,values) {
        Gp = values["Type"];


      });
    });*/

  }

  _onEntryAdded(Event event) {
    setState(() {
      msgList.add(Item.fromSnapshot(event.snapshot));
      print(" cjjjjjjjjjjj ");
    });
  }

  Widget appBarTitle = new Text("Messages");
  Icon actionIcon = new Icon(Icons.search);
  Icon CreateG = new Icon(Icons.group_add);
  bool visible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      /*appBar: new AppBar(
          actions: <Widget>[
           // new IconButton(icon: Icon(Icons.person), onPressed: null),

            new IconButton(icon: actionIcon,onPressed:(){
              setState(() {

                if ( (this.actionIcon.icon == Icons.search)){
                  this.actionIcon = new Icon(Icons.close);
                  this.appBarTitle = new TextField(

                    style: new TextStyle(fontSize: 20.0,
                      color: Colors.white,

                    ),
                    decoration: new InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: new Icon(Icons.search,color: Colors.white),
                        hintText: "Search...",
                        hintStyle: new TextStyle(color: Colors.white)
                    ),
                  );}


              });
            },
            ),

          ]
      ),*/


      appBar: visible == false? AppBar(
        title: Text('Message List'),
        backgroundColor:Colors.deepPurpleAccent,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.group_add),
            onPressed: (){
              visible=true;
              //_showDialog();

            },
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              setState(() {
                visible = true;
              });
//              Navigator.push(
//                context,
//                MaterialPageRoute(
//                  builder: (BuildContext context) => new HomePage(),
//                ),
//              );
            },
          ),

          PopupMenuButton<String>(
            onSelected: (String value){},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'))

              ];
            },
          ),
        ],
      )
      :AppBar(
          title: Text('Message List'),
          backgroundColor:Colors.deepPurpleAccent,
          actions: <Widget>[
              IconButton(
              icon: Icon(Icons.group_add),
              onPressed: (){
                _showDialog();

              },
            ),

          ],
      ),

      //resizeToAvoidBottomPadding: true,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Contact()),
            //MaterialPageRoute(builder: (context) => Contact()),
          );
        },
        child: Icon(Icons.message,size: 30.0,),
        //child: Icon(Icons.send,size: 30.0,),
        elevation: 0.0,
        backgroundColor: Colors.deepPurpleAccent,
      ),

      body:
      // Container(child: new Text("text"))
      Column(
        children: <Widget>[
          new Divider(height: 08.1),
//          Center( child: new Text(" sssssssssss $OwnerName")),
          // shadow
          /*     new Text("Shadow",
            style: TextStyle(
                shadows: <Shadow>[
                  Shadow(
                    // offset: Offset(2.0, 2.0),
                    blurRadius: 5.0,
                    color: Color.fromARGB(255, 0, 0, 0),
                  ),
                  *//*Shadow(
                  offset: Offset(10.0, 10.0),
                  blurRadius: 8.0,
                  color: Color.fromARGB(125, 0, 0, 255),
                ),*//*
                ]),),*/
          // new Text("contact list"),
          new Flexible(

              child:  FirebaseAnimatedList(
                query: itemRefM.orderByChild("message"),
                /* sort: _anchorToBottom
                    ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                    : null,*/
                // itemCount: UsersList.length,
                padding: new EdgeInsets.fromLTRB(10, 10.0, 10.0, 0.0),
                reverse: false,
               // sort: (a, b) => b.key.compareTo(a.key),
//                sort: (DataSnapshot a, DataSnapshot b) =>
//                    b.key.compareTo(a.key),
                sort: (a, b) => b.value['message'].toString().compareTo(a.value['message'].toString()),
                itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation,int index) {
                  return Slidable(
                    delegate: new SlidableDrawerDelegate(),
                    actionExtentRatio: 0.25,

                    child: Container(
                      decoration: new BoxDecoration(boxShadow: [
                        new BoxShadow(
                          color: Colors.black12,
                          blurRadius: 5.0,
                        ),
                      ]),

                      /*   decoration: new BoxDecoration(
                        color: Colors.purple,
                        gradient: new LinearGradient(
                          colors: [Colors.red, Colors.cyan],
                        ),
                      ),*/

                      child: new Card(

                        shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        color: mycolor,
                       // color: Colors.white,

                        child: Column(mainAxisSize: MainAxisSize.min,
                          children: <Widget>[

                         new  ListTile(
                             selected: isSelected,
                             //// sizeFactor: animation,
                           leading: new CircleAvatar(
                             child: new Text(snapshot.key.toUpperCase().substring(0, 2)),
                           ),
                             /*leading:
                             new CircleAvatar(
                               child:  new Image.network("http://res.cloudinary.com/kennyy/image/upload/v1531317427/avatar_z1rc6f.png"),
                             ),*/

                             //Icon(Icons.person),
                             title: Text(snapshot.key.toString()),
                             subtitle: Text(snapshot.key.toString() , style: TextStyle(fontSize: 18.0),),
                             //subtitle:Text(one(snapshot.key.toString())),//Text(msgList[index].message.toString(), style: TextStyle(fontSize: 18.0),),
                             // itemCount: MsgList.length,  snapshot.value.toString()

                            // trailing: new FlatButton.icon(onPressed: null, icon: Icon(Icons.cloud_done), label: null),
                             trailing: const Icon(Icons.arrow_forward_ios),
                             onTap: ()
                             {
                               you = snapshot.key.toString();
                               final toMessage = "$you";
                               print("click");
                               Grp_Prn(toMessage);
                               /*Navigator.push(
                               context,
                               MaterialPageRoute(
                                 builder: (BuildContext context) => new messageHere(value2: "${widget.value1}", value1: toMessage),
                               ),
                             );*/
                             },
                             onLongPress: (){ setState(() { mycolor=Colors.red;});//_showDialog();
                             //  select();
                               //toggleSelection();
                               /* setState(() {
                                 selected = !selected;
                                 print("selected");
                               });*/
//                                setState(() {
//                                  if (isSelected) {
//                                    print("selected");
//                                    mycolor=Colors.white;
//                                    //border=new BoxDecoration(border: new Border.all(color: Colors.white));
//                                    isSelected = false;
//                                  } else {
//                                    mycolor=Colors.grey[300];
//                                    isSelected = true;
//                                  }
//                                });
                               //  itemRef.onChildRemoved.listen(_onEntryRemoved);
                             },
                           ),
                          ],
                        ),



                      ),

                    ),

                    actions: <Widget>[
                      new IconSlideAction(
                        caption: 'Archive',
                        color: Colors.blue,
                        icon: Icons.archive,
                        onTap: () => _showSnackBar('Archive', you),
                      ),
                      /*new IconSlideAction(
                        caption: 'Share',
                        color: Colors.indigo,
                        icon: Icons.share,
                        //  onTap: () => _showSnackBar('Share'),
                      ),*/
                    ],
                    secondaryActions: <Widget>[
                      /*new IconSlideAction(
                        caption: 'More',
                        color: Colors.black45,
                        icon: Icons.more_horiz,
                        //      onTap: () => _showSnackBar('More'),
                      ),*/
                      new IconSlideAction(
                        caption: 'like',
                        color: Colors.green,
                        icon: Icons.favorite,
                        //      onTap: () => _showSnackBar('More'),
                      ),
                      new IconSlideAction(
                          caption: 'Delete',
                          color: Colors.red,
                          icon: Icons.delete,
                          onTap: (){
                            print("$you");
                          }// => _showSnackBar('Delete',you),
                      ),

                    ],

                  );
                },

              )
          ),

          //new Padding(padding: null),
          /* Padding(
            padding: const EdgeInsets.all(8.0),
            child: new FloatingActionButton.extended(onPressed: null, icon: Icon(Icons.add), label: null),
          ),*/
          /*new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.add,size: 35.0,),
            backgroundColor: Colors.deepPurpleAccent,
//            onPressed: _isWriting
//                ? () => _submitMsg(_textController.text)
//                : null,
          ),
          new Divider(height: 5.1),
*/
        ],

      ),
    );
  }



  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
  }

  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Create A Group",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: new TextField(
            controller: GrpController,
            style: TextStyle(fontSize: 20.0,color: Colors.black,
                fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
            maxLength: 20,textAlign: TextAlign.start,decoration: InputDecoration
            (labelText: "Enter Group name", ),), //***labelStyle:
          actions: <Widget>[
            new FlatButton(
              child: new Text("Save",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),

              onPressed: () {
                String Grp =GrpController.text;
                Navigator.of(context).pop();
                GrpController.clear();
                Navigator.push(
                  context,
                  MaterialPageRoute(

                    builder: (BuildContext context) => new Group(grpName: Grp, isMe: "${widget.value1}"),
                  ),
                );

              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cancel",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void Grp_Prn(String toMessage) async{

    itemRefMType=FirebaseDatabase.instance.reference().child("messagalu").child("${widget.value1}").child("$toMessage");
    itemRefMType.once().then((DataSnapshot snapshot){
      Map<dynamic, dynamic> values=snapshot.value;
      values.forEach((key,values) {
        String Gp = values["Type"];
        print(Gp);
        String gp ="Group";


        Navigator.push(
          context,
          MaterialPageRoute(

            builder: gp != values["Type"] ? (BuildContext context) => new messageHere(value2: "${widget.value1}", value1: toMessage)
                : (BuildContext context) => new messageGroup(value2: "${widget.value1}", value1: toMessage),
          ),
        );
      });
    });

  }

  /*one(String u) {
//    itemRefMType=FirebaseDatabase.instance.reference().child("messagalu").child("${widget.value1}").child("$u");
//    itemRefMType.once().then((DataSnapshot snapshot){
//      Map<dynamic, dynamic> values=snapshot.value;
//      values.forEach((key,values) {
    //      String Gp = values["message"];
    String v ="okk $u";
//    print("vvvvvvvvvv $v");
//    print(twwo(v));
//
//    print(u);
//       String gp ="Group";
    return(v);

//      });
//    });
  }*/

  two(Type string) {
    String s ="send";
    return(s);

  }

  _showSnackBar(String s, String you) {
    switch(s){
      case "Archive":
        print("work : $you");
        return Colors.red;
      case "Delete":
        print("word");
        return Colors.red;

    }
  }
  void toggleSelection() {
    setState(() {
      if (isSelected) {
        mycolor=Colors.white;
        isSelected = false;
      } else {
        mycolor=Colors.grey[300];
        isSelected = true;
      }
    });
  }

  void select() {
    setState(() {
      if (SelectedState) {
        //border=new BoxDecoration(border: new Border.all(color: Colors.white));
        mycolor = Colors.white;
        SelectedState = false;
      } else {
       // border=new BoxDecoration(border: new Border.all(color: Colors.grey));
        mycolor = Colors.grey[300];
        SelectedState = true;
      }
    });
  }

/* one()async {

    //DatabaseReference databaseReference = Firebase.getInstance().getReference();
    Query lastQuery = itemRefM.orderByKey().limitToLast(1);
    lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
    String message = dataSnapshot.child("message").getValue().toString();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
    //Handle possible errors.
    }
    });
  }*/


}

String one(String string)  {
  String bay= "kkk $string";
  return(bay);
}

twwo(String v) async {

  try {
    DatabaseReference itemRefM= await FirebaseDatabase.instance.reference().child("messagalu").child("$v").child("$v");

    String one ="print";
    return(one);
  } catch (e) {
    print(e.toString());
  }



}

/*void Grp_Prn(String toMessage) async {
  print("mmmmmmmmm $toMessage");
  // single value retrieve
  await FirebaseDatabase.instance.reference().child("messagalu").child("$OwnerName").child("details").child("name").once().then((
      DataSnapshot snapshot) {
    _objdatabase = snapshot.value;
    OwnerName = _objdatabase;
    print("owner : $OwnerName");
  });



}*/

//****** List of item *******\\
class Item {
  String key;
  String message;
  String sender;


  Item(this.message, this.sender);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        message = snapshot.value["message"],
        sender = snapshot.value["sender"];

  toJson() {
    return {
      "message": message,
      "sender": sender,
      "key": key
    };
  }
}

class _HomeItem {
  const _HomeItem(
      this.index,
      this.title,
      this.subtitle,
      this.color,
      );

  final int index;
  final String title;
  final String subtitle;
  final Color color;
}



///******* last query https://stackoverflow.com/questions/41601147/get-last-node-in-firebase-database-android
/*

DatabaseReference databaseReference = Firebase.getInstance().getReference();
Query lastQuery = databaseReference.child("mp").orderByKey().limitToLast(1);
lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
  @Override
  public void onDataChange(DataSnapshot dataSnapshot) {
  String message = dataSnapshot.child("message").getValue().toString();
  }

  @Override
  public void onCancelled(DatabaseError databaseError) {
  //Handle possible errors.
  }
});

*/

// push notification


/*class _TestState extends State<Test> {
  double rating = 3.5;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new ListView(
        children: ListTile
            .divideTiles(
          context: context,
          tiles: new List.generate(42, (index) {
            return new SlideMenu(
              child: new ListTile(
                title: new Container(child: new Text("Drag me")),
              ),
              menuItems: <Widget>[
                new Container(
                  child: new IconButton(
                    icon: new Icon(Icons.delete),
                  ),
                ),
                new Container(
                  child: new IconButton(
                    icon: new Icon(Icons.info),
                  ),
                ),
              ],
            );
          }),
        )
            .toList(),
      ),
    );
  }
}*/
