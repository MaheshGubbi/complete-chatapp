import 'dart:async';
import 'dart:io';
import 'package:firebase/forward.dart';
import 'package:firebase/storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
//import 'package:video_player/video_player.dart';
 final ThemeData iOSTheme = new ThemeData(
   primarySwatch: Colors.red,
   primaryColor: Colors.grey[400],
   primaryColorBrightness: Brightness.dark,
 );

 final ThemeData androidTheme = new ThemeData(
   primarySwatch: Colors.blue,
   accentColor: Colors.green,
 );

 String defaultUserName;

class messageHere extends StatefulWidget {
  final String value1;
  final String value2;

  messageHere({Key key, this.value1, this.value2,}) : super(key: key);

  @override
  _messageHereState createState() => _messageHereState();
}
 class _messageHereState extends State<messageHere> {
   var mycolor=Colors.deepPurpleAccent;
   var mycolor1=Colors.white;
   String user_Name;
   String userId;
   String isMe;
   String you;
   String meANDyou;
   String me;
   int test = 40;
   DatabaseReference databaseId;
   StorageReference firebaseStorageRef;
   DatabaseReference itemRef;
   DatabaseReference databaseMsg;
   DatabaseReference databaseId1;
   final TextEditingController _textController = new TextEditingController();
   bool _isWriting = false;
   bool change = false;
   String delete = null;
   String forward = null;
   File Image1 = null;
   File ImgPath = null;
   StorageUploadTask Task = null;
   String URL = null;




   List<Item> UsersList = List();
   Item item;



   /*Future getImage() async {
     var image = await ImagePicker.pickImage(source: ImageSource.gallery);

     setState(() {
       _image = image;
     });
   }*/

   @override
   void initState() {
     super.initState();
    // _init();
     item = Item("", "");
     you = "${widget.value1}";
     print("R you :$you") ;
     isMe = "${widget.value2}";
     print("R meeeeee : $isMe");
     databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");

     databaseId1 = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");
     databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$you").child("$isMe");
     final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.
     itemRef = database.reference().child('User');
     itemRef.onChildAdded.listen(_onEntryAdded);
     itemRef.onChildRemoved.listen(_onEntryRemoved);
     itemRef.onChildChanged.listen(_onEntryChanged);
   }


   Future _init() async {
     await FirebaseDatabase.instance.setPersistenceEnabled(true);
     Object _objdatabase;
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     print("pkkkkkkkk");
     print("user Id nnn $userId");
     await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
         DataSnapshot snapshot) {
       _objdatabase = snapshot.value;
       user_Name = _objdatabase;
       defaultUserName =user_Name;

       String dash = "_${widget.value1}";
       meANDyou = "$user_Name$dash";
       isMe = "$user_Name";
       print("meeeeeeeee : $isMe");
       print("youuuuuuuu : ${widget.value1}");

       final ref = FirebaseStorage.instance.ref().child('testimage');
    //  // databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");
       // print("name : $OwnerName");
     });

   }

   _onEntryAdded(Event event) {
     setState(() {
       UsersList.add(Item.fromSnapshot(event.snapshot));
     });
   }

   _onEntryChanged(Event event) {
     var old = UsersList.singleWhere((entry) {
       return entry.key == event.snapshot.key;
     });
     setState(() {
       UsersList[UsersList.indexOf(old)] = Item.fromSnapshot(event.snapshot);
     });
   }

   _onEntryRemoved(Event event) {
     setState(() {
       UsersList.remove(Item.fromSnapshot(event.snapshot));
     });
   }


   @override
   Widget build(BuildContext ctx) {
     //final bg = Colors.greenAccent.shade100;
     final bg = Colors.deepPurpleAccent;
     //final icon = send1 ? Icons.done_all : Icons.done;
     //final bg = isMe ? Colors.white : Colors.greenAccent.shade100;

     /*final radius = isMe
         ? BorderRadius.only(
       topRight: Radius.circular(5.0),
       bottomLeft: Radius.circular(10.0),
       bottomRight: Radius.circular(5.0),
     )
         : BorderRadius.only(
       topLeft: Radius.circular(5.0),
       bottomLeft: Radius.circular(5.0),
       bottomRight: Radius.circular(10.0),
     );*/

     return new Scaffold(
       appBar:change == false ? new AppBar(

         title: new Text("${widget.value1}"),
         backgroundColor: Colors.deepPurpleAccent,
         elevation:
         Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
       )
       :new AppBar(

         title: new Text("change"),
         backgroundColor: Colors.deepPurpleAccent,
         elevation:
         Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
         actions: <Widget>[
           // action button
           IconButton(
             icon: Icon(Icons.delete),
             onPressed: () {
               databaseMsg.child(delete).remove();
               setState(() {
                 change =false;
               });
               },
           ),
           // action button
           IconButton(
             padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
             icon: Icon(Icons.near_me),
             onPressed: () {
               Navigator.push(
                 context,
                 MaterialPageRoute(
                   builder: (BuildContext context) => new Forward(isMe :"${widget.value2}", tomsg: "${widget.value1}", message : forward),
                 ),
               );
               //_select(choices[1]);
             },
           ),
           // overflow menu

         ],
       ),


        /* body: SafeArea(child: AnimatedPage(
     appearance: _arc ? HeaderAppearance.arc : HeaderAppearance.profile,
         backgroundImage: _arc ? 'assets/one.jpg' : 'assets/one.jpg',*/

         body:  new Container(
           child: new Column
             (
             //crossAxisAlignment: CrossAxisAlignment.stretch,

             children: <Widget>[
            new Text(" ONE TO ONE ",style: TextStyle(fontSize: 20.0),),

           Flexible(
             child: FirebaseAnimatedList(
               sort: (DataSnapshot a, DataSnapshot b) =>
                   b.key.compareTo(a.key),
             query: databaseMsg,

           //  padding: new EdgeInsets.all(10.0),
//               padding: new EdgeInsets.fromLTRB(5.0, 10.0, 0.0, 0.0);

               reverse: true,
               itemBuilder: (context, snapshot, animation, index) {
                 return  Padding(

                   //padding: new EdgeInsets.all(10.0),
//                   padding: isMe == snapshot.value['sender']? const EdgeInsets.fromLTRB(5.0, 10.0, 150.0, 0.0)
//                       :const EdgeInsets.fromLTRB(150.0, 10.0, 2.0, 0.0),

                   //******* separate sender and receiver  messages*******\\
                 padding: isMe == snapshot.value['sender']? const EdgeInsets.fromLTRB(25.0, 10.0, 150.0, 0.0)
                       :const EdgeInsets.fromLTRB(150.0, 10.0, 25.0, 0.0),
                   child: Column(
                     crossAxisAlignment: isMe == snapshot.value['sender']? CrossAxisAlignment.start: CrossAxisAlignment.end,

                     children: <Widget>[
                       new Text(snapshot.value['sender'],//textAlign: TextAlign.left,
                           style: new TextStyle(
                           color: Colors.blueAccent,
                           fontSize: 18.0,
                           fontWeight: FontWeight.w900)),
                       new Container(

                       margin: const EdgeInsets.all(3.0),
                         padding: const EdgeInsets.all(8.0),
                         decoration: BoxDecoration(
                           boxShadow: [
                             BoxShadow(
                                 blurRadius: .5,
                                 spreadRadius: 1.0,
                                 color: Colors.black.withOpacity(.12))
                             //color: Colors.indigo.withOpacity(.12))

                           ],
                           color:isMe == snapshot.value['sender']? Colors.white : Colors.deepPurpleAccent, //********
                           //borderRadius: new BorderRadius.circular(5.0),

                           borderRadius: isMe == snapshot.value['sender']? new BorderRadius.only(
                             topRight: Radius.circular(10.0),
                             bottomLeft: Radius.circular(20.0),
                             bottomRight: Radius.circular(10.0),
                           ): BorderRadius.only(
                             topLeft: Radius.circular(10.0),
                             bottomLeft: Radius.circular(10.0),
                             bottomRight: Radius.circular(20.0),
                           ),
                         ),
                       child:

                       GestureDetector(
                         // new Image.file(Image1,),
                         child: snapshot.value['message']== null
                               // ? new Image.network(URL)
                                ?new Image.file(Image1,)
                                : new Text(snapshot.value['message'],textAlign: TextAlign.center,
                             style:
                             new TextStyle(
                                 color: isMe == snapshot.value['sender']?Colors.deepPurpleAccent : Colors.white,
                                 fontSize: 16.0,
                                 fontWeight: FontWeight.w900)),
                         onLongPress: () {
                           setState(() {
                               //mycolor1=Colors.red;
                               final snackBar = SnackBar(content: Text(snapshot.value['message']+ " is selected",style: TextStyle(color: Colors.red),));
                               Scaffold.of(context).showSnackBar(snackBar);

                             //mycolor1=Colors.red;
                            // mycolor= isMe == snapshot.value['sender']? Colors.red : Colors.red;
                             change = true;
                             delete = snapshot.key;
                             forward = snapshot.value['message'];
                             });

                           print(snapshot.value['message']);
                         },
                       ),
                       ),
                     ],
                   ),
                 );
               },
           ),
           ),

          // new Text("save ${widget.value2}"),
               new Divider(height: 1.0),

               new Container(
                 child: _buildComposer(),

                 decoration: new BoxDecoration(color: Theme.of(ctx).cardColor),

               ),
             ],
           ),
           decoration: Theme.of(context).platform == TargetPlatform.iOS
               ? new BoxDecoration(
               border: new Border(
                   top: new BorderSide(
                     color: Colors.grey[200],
                   )))
               : null,
         )
     );
   }


   Widget _buildComposer() {
     return new IconTheme(


//         border: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(32.0)
//         ),
       data: new IconThemeData(color: Theme.of(context).accentColor),
       child: new Container(
         color: Colors.white,
         margin: const EdgeInsets.symmetric(horizontal: 9.0),

           child: new Row(

             children: <Widget>[

               new Container(
                 margin: new EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
                 child: new IconButton(
                     padding: new EdgeInsets.fromLTRB(0.0, 0, 2.0, 0),
                     icon: new Icon(Icons.camera, size: 30.0,),
                     onPressed: (){
                       print("test");

                       picker();
                     }// => _handleSubmitted(_textController.text)
               ),
               ),
               new Flexible(
                 child: new TextField(
                   maxLines: null,
                   keyboardType: TextInputType.multiline,
                   controller: _textController,
                   onChanged: (String txt) {
                     setState(() {
                       _isWriting = txt.length > 0;
                     });
                   },
                   onSubmitted: _submitMsg,
                   decoration: InputDecoration(
                       /*border: OutlineInputBorder(
                           borderRadius: BorderRadius.circular(32.0)
                       )*/
                       //contentPadding: new EdgeInsets.symmetric(vertical: 0.0),
                      // border: InputBorder.none,
                      /* prefixIcon: Padding(
                         padding: EdgeInsets.all(0.0),
                         child: Icon(
                           Icons.camera,
                           color: Colors.grey,
                             //onPressed:(){},
                         ),
                         // icon is 48px widget.
                       ),*/
                 /*suffixIcon:Padding(
                   padding: EdgeInsets.all(0.0),
                   child: Icon(
                     Icons.attachment,
                     color: Colors.grey,
                   ), // icon is 48px widget.
                 ),*/
                 border: InputBorder.none,
                 hintText: "Write a Message"),
                 ),

               ),
               new Container(
                // margin: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0),
                 child: new IconButton(
                     padding: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                     icon: new Icon(Icons.attachment, size: 30.0,),
                     alignment: FractionalOffset.centerLeft,
                     tooltip: 'send image',
                     onPressed: (){
//                       Navigator.push(
//                         context,
//                         MaterialPageRoute(
//                           builder: (BuildContext context) => new Store(),
//                         ),
//                       );

                       picker1();
                     }// => _handleSubmitted(_textController.text)
                 ),
               ),


               new FloatingActionButton(
                 //  elevation: 0.0,

                   child: new Icon(Icons.near_me),

                   backgroundColor: Colors.deepPurpleAccent,

                   onPressed: (){_submitMsg(_textController.text);}
                   /*_isWriting
                       ? () => _submitMsg(_textController.text)
                       : null,*/
               ),
             /*  new Container(
                   margin: new EdgeInsets.symmetric(horizontal: 3.0),
                   child: Theme.of(context).platform == TargetPlatform.iOS
                       ? new CupertinoButton(
                       child: new Text("Submit"),
                       onPressed: _isWriting ? () => _submitMsg(_textController.text)
                           : null
                   )
                       : new IconButton(
                     icon: new Icon(Icons.send),
                     onPressed: _isWriting
                         ? () => _submitMsg(_textController.text)
                         : null,
                   )
               ),*/
             ],
           ),
           decoration: Theme.of(context).platform == TargetPlatform.iOS
               ? new BoxDecoration(
               border:
               new Border(top: new BorderSide(color: Colors.brown))):null,
       ),
     );
   }

   void _submitMsg(String text) {
     print("working");
   //  uName();
     _sendMessage(sender:isMe, messageText: text, imageUrl: null);

     _textController.clear();
     setState(() {
       _isWriting = false;
     });
   }
   void _submitImg(String uRl) {
     print("working");
     //  uName();
     _sendMessage(sender:isMe, messageText: null, imageUrl: uRl);

     _textController.clear();
     setState(() {
       _isWriting = false;
     });
   }

   void _sendMessage({ String sender,String messageText, String imageUrl}) {
     print("working111");
     print("url : $imageUrl");
     databaseId.push().set({
       'message': messageText,
       'sender' : sender,
       'imageUrl': imageUrl
     });
     databaseId1.push().set({
       'message': messageText,
       'sender' : sender,
       'imageUrl':imageUrl
     });
     }

// retrieve user ID
   void nameId() async {
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
     databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$user_Name").child("$meANDyou");


 }

   void uName() async {
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     userId = user.uid;
     Object _objdatabase;

     // single value retrieve
     await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
         DataSnapshot snapshot) {

       _objdatabase = snapshot.value;
       user_Name = _objdatabase;
       defaultUserName =user_Name;
       isMe = defaultUserName;
       you = "_${widget.value1}";
      // String dash = "_${widget.value1}";
       //String dash1 = "_$user_Name";
       you = "${widget.value1}";
       print("yyyyyyyyyy $you");
       print("wwwwwww  $isMe");

       //meANDyou="$user_Name$dash";
       //String youANDme="${widget.value1}$dash1";
      databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");
       //databaseId1 = FirebaseDatabase.instance.reference().child("messagalu").child("$you").child("$youANDme");
      databaseId1 = FirebaseDatabase.instance.reference().child("messagalu").child("$you").child("$isMe");
       });
   }

  getSentMessageLayout(DataSnapshot messageSnapshot) {

  }

  getReceivedMessageLayout(DataSnapshot messageSnapshot) {

  }

  sender() {}

  receiver() {}

   void picker() async {
     print("camera");
     File img = await ImagePicker.pickImage(source: ImageSource.camera) as File;
     print(img.path);
     File Image = img;
     //_submitMsg(img);
     if (img != null) {

       setState(() {
        // _sendMessage(sender:isMe, messageText: img, imageUrl: null);

       });
     }
   }
   void picker1() async {
     print("camera");
     int timestamp = new DateTime.now().millisecondsSinceEpoch;
     File img = await ImagePicker.pickImage(source: ImageSource.gallery) as File;
     String  ImgPath= img.path;
     print(ImgPath);
     //File Image1 = img;
     if (img != null) {

       setState(() {
         Image1=img;
       });
      // _showDialog();
       showDialog(
         context: context,
         builder: (BuildContext context) {
           // return object of type Dialog
           return AlertDialog(

             //title: new Text("Create A Group",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),

             content: new Image.file(Image1,),
             //***labelStyle:
             actions: <Widget>[
               new FlatButton(
                 child: new Text("Cancel",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
                 onPressed: () {
                   Navigator.of(context).pop();
                 },
               ),
               new FlatButton(
                 child: new Text("send",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),

                 onPressed: () {
                   final StorageReference firebaseStorageRef =
                   FirebaseStorage.instance.ref().child("group").child("img_" + timestamp.toString() + ".jpg");
                   final StorageUploadTask task = firebaseStorageRef.putFile(Image1);
                   print("upload");
//                   String url = (firebaseStorageRef.getDownloadURL()).toString();
//                   print(url);

                   setState(() {
                     Task = task;
                     printUrl();
                   });
                   // String url = (await firebaseStorageRef.getDownloadURL()).toString();
                   // print("url : $url");
                   Navigator.of(context).pop();
                   _submitImg(URL);

                   /*Navigator.push(
                   context,
                   MaterialPageRoute(

                     builder: (BuildContext context) => new Group(),
                   ),
                 );*/

                 },
               ),
               // usually buttons at the bottom of the dialog

             ],
           );
         },
       );
     }
   }

   printUrl() async {
     var dowurl = await (await Task.onComplete).ref.getDownloadURL();
     String url = dowurl.toString();
     print("okkkkkkkkkkkkk $url");
     setState(() {
       URL = url;
     });
     return(url);
   }

   void _showDialog() {
     // flutter defined function
     showDialog(
       context: context,
       builder: (BuildContext context) {
         // return object of type Dialog
         return AlertDialog(
           
           //title: new Text("Create A Group",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
           
           content: new Image.file(Image1,),
           //***labelStyle:
           actions: <Widget>[
             new FlatButton(
               child: new Text("Cancel",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
               onPressed: () {
                 Navigator.of(context).pop();
               },
             ),
             new FlatButton(
               child: new Text("send",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),

               onPressed: () {
                 final StorageReference firebaseStorageRef =
                 FirebaseStorage.instance.ref().child("group").child('one.jpg');
                 final StorageUploadTask task = firebaseStorageRef.putFile(Image1);
                 print("upload");

                 setState(() {
                   //Task = task;
                   printUrl();
                 });
                // String url = (await firebaseStorageRef.getDownloadURL()).toString();
                // print("url : $url");
                 Navigator.of(context).pop();

                 /*Navigator.push(

                   context,
                   MaterialPageRoute(

                     builder: (BuildContext context) => new Group(),
                   ),
                 );*/

               },
             ),
             // usually buttons at the bottom of the dialog

           ],
         );
       },
     );
   }

 }
 class myData {
   String name, message, profession;

   myData(this.name, this.message, this.profession);
 }

 class Item {
   String key;
   String sender;
   String message;
  // String imageUrl;

   Item(this.sender, this.message);

   Item.fromSnapshot(DataSnapshot snapshot)
       : key = snapshot.key,
         sender = snapshot.value["sender"],
         message = snapshot.value["message"];

   toJson() {
     return {
       "sender": sender,
       "message": message,
     };
   }
 }




 // arc
 /*

 enum HeaderAppearance { arc, profile }

 double _getTargetMaxExtent(HeaderAppearance appearance) {
   if (appearance == HeaderAppearance.arc) {
     return 150.0;
   } else {
     return 75.0;
   }
 }

 double _getTargetArcAnimationValue(HeaderAppearance appearance) {
   if (appearance == HeaderAppearance.arc) {
     return 1.0;
   } else {
     return 0.0;
   }
 }

 class AnimatedPage extends StatefulWidget {
   AnimatedPage({Key key, this.appearance, this.backgroundImage, this.children}) : super(key: key);

   final HeaderAppearance appearance;
   final String backgroundImage;
   final List<Widget> children;

   @override
   _AnimatedPageState createState() => _AnimatedPageState();
 }

 class _AnimatedPageState extends State<AnimatedPage> with SingleTickerProviderStateMixin {
   AnimationController _maxExtentAnimation;

   @override
   void initState() {
     super.initState();
     _maxExtentAnimation = AnimationController.unbounded(vsync: this, value: _getTargetMaxExtent(widget.appearance));
   }

   @override
   void didUpdateWidget(AnimatedPage oldWidget) {
     super.didUpdateWidget(oldWidget);
     if (widget.appearance != oldWidget.appearance) {
       _maxExtentAnimation.animateTo(
         _getTargetMaxExtent(widget.appearance),
         duration: Duration(milliseconds: 600),
         curve: Curves.easeInOut,
       );
     }
   }

   @override
   void dispose() {
     _maxExtentAnimation.dispose();
     super.dispose();
   }

   @override
   Widget build(BuildContext context) {
     return AnimatedBuilder(
       animation: _maxExtentAnimation,
       builder: (context, child) {
         return CustomScrollView(
           slivers: <Widget>[
             SliverPersistentHeader(
               pinned: true,
               delegate: AnimatedHeaderDelegate(
                 appearance: widget.appearance,
                 backgroundImage: widget.backgroundImage,
                 minExtent: 50.0,
                 maxExtent: _maxExtentAnimation.value,
               ),
             ),
             child,
           ],
         );
       },
       child: SliverList(delegate: SliverChildListDelegate(widget.children)),
     );
   }
 }

 class AnimatedHeaderDelegate extends SliverPersistentHeaderDelegate {
   AnimatedHeaderDelegate({this.appearance, this.backgroundImage, this.minExtent, this.maxExtent});

   final HeaderAppearance appearance;

   final String backgroundImage;

   @override
   final double minExtent;
   @override
   final double maxExtent;

   @override
   Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
     final shrinkRelative = shrinkOffset / (maxExtent - minExtent);
     return AnimatedHeader(
       appearance: appearance,
       backgroundImage: backgroundImage,
       curvatureMultiplier: 1.0 - shrinkRelative,
     );
   }

   @override
   bool shouldRebuild(AnimatedHeaderDelegate oldDelegate) {
     return appearance != oldDelegate.appearance ||
         minExtent != oldDelegate.minExtent ||
         maxExtent != oldDelegate.maxExtent;
   }
 }

 class AnimatedHeader extends StatefulWidget {
   AnimatedHeader({Key key, this.appearance, this.backgroundImage, this.curvatureMultiplier}) : super(key: key);

   final HeaderAppearance appearance;

   final String backgroundImage;

   final double curvatureMultiplier;

   @override
   _AnimatedHeaderState createState() => _AnimatedHeaderState();
 }

 class _AnimatedHeaderState extends State<AnimatedHeader> with TickerProviderStateMixin {
   AnimationController _arcAnimation;

   @override
   void initState() {
     super.initState();
     _arcAnimation = AnimationController(
       vsync: this,
       value: _getTargetArcAnimationValue(widget.appearance),
       duration: Duration(milliseconds: 600),
     );
   }

   @override
   void didUpdateWidget(AnimatedHeader oldWidget) {
     super.didUpdateWidget(oldWidget);
     if (widget.appearance != oldWidget.appearance) {
       _arcAnimation.animateTo(_getTargetArcAnimationValue(widget.appearance));
     }
   }

   @override
   Widget build(BuildContext context) {
     return AnimatedBuilder(

       animation: CurvedAnimation(parent: _arcAnimation, curve: Curves.linear),
       builder: (context, child) {
         return ClipPath(
           clipper: ArcClipper(
             curvature: _arcAnimation.value * widget.curvatureMultiplier,
           ),
           clipBehavior: Clip.antiAlias,
           child: child,
         );
       },
       child: Stack(
         fit: StackFit.expand,
         children: <Widget>[
           AnimatedSwitcher(
             duration: Duration(milliseconds: 600),
             child: Container(
               key: ValueKey(widget.backgroundImage),
               decoration: BoxDecoration(
                 image: DecorationImage(
                   image: AssetImage(widget.backgroundImage),
                   fit: BoxFit.cover,
                 ),
               ),
             ),
           ),
           Center(
             child: Text(
               'TITLE',
               style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w500),
             ),
           ),
         ],
       ),
     );
   }
 }

 class ArcClipper extends CustomClipper<Path> {
   ArcClipper({this.curvature});

   final double curvature;

   @override
   Path getClip(Size size) {
     if (curvature == 0.0) {
       return Path()..addRect(Offset.zero & size);
     } else {
       return Path()
         ..moveTo(0.0, 0.0)
         ..lineTo(size.width, 0.0)
         ..lineTo(size.width, size.height)
         ..quadraticBezierTo(size.width / 2, size.height - size.height * 0.4 * curvature, 0.0, size.height)
         ..close();
     }
   }

   @override
   bool shouldReclip(ArcClipper oldClipper) {
     return curvature != oldClipper.curvature;
   }
 }

 */
