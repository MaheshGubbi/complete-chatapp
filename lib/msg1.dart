import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final ThemeData iOSTheme = new ThemeData(
  primarySwatch: Colors.red,
  primaryColor: Colors.grey[400],
  primaryColorBrightness: Brightness.dark,
);

final ThemeData androidTheme = new ThemeData(
  primarySwatch: Colors.blue,
  accentColor: Colors.green,
);

String defaultUserName;

class messageH extends StatefulWidget {
  final String value1;
  final String value2;

  messageH({Key key, this.value1, this.value2,}) : super(key: key);

  @override
  _messageHereState createState() => _messageHereState();
}
class _messageHereState extends State<messageH> {
  String user_Name;
  String userId;
  String isMe;
  String you;
  String meANDyou;
  String me;
  int test = 40;
  DatabaseReference databaseId;
  DatabaseReference itemRef;
  DatabaseReference databaseMsg;
  DatabaseReference databaseId1;
  final TextEditingController _textController = new TextEditingController();
  bool _isWriting = false;



  List<Item> UsersList = List();
  Item item;

  @override
  void initState() {
    super.initState();
    // _init();
    item = Item("", "");
    you = "${widget.value1}";
    print("R you :$you") ;
    isMe = "${widget.value2}";
    print("R meeeeee : $isMe");
    databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");

    databaseId1 = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe").child("$you");
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$you").child("$isMe");
    final FirebaseDatabase database = FirebaseDatabase.instance; //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    }



  @override
  Widget build(BuildContext ctx) {
    //final bg = Colors.greenAccent.shade100;
    final bg = Colors.deepPurpleAccent;

    return new Scaffold(
        appBar: new AppBar(

          title: new Text("${widget.value1}"),
          backgroundColor: Colors.deepPurpleAccent,
          elevation:
          Theme.of(ctx).platform == TargetPlatform.iOS ? 0.0 : 6.0,
        ),


        /* body: SafeArea(child: AnimatedPage(
     appearance: _arc ? HeaderAppearance.arc : HeaderAppearance.profile,
         backgroundImage: _arc ? 'assets/one.jpg' : 'assets/one.jpg',*/

        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: FirebaseAnimatedList(
                sort: (DataSnapshot a, DataSnapshot b) =>
                    b.key.compareTo(a.key),
                query: databaseMsg,

                reverse: true,
                itemBuilder: (context, snapshot, animation, index) {
                  return  Padding(

                    //padding: new EdgeInsets.all(10.0),
//                   padding: isMe == snapshot.value['sender']? const EdgeInsets.fromLTRB(5.0, 10.0, 150.0, 0.0)
//                       :const EdgeInsets.fromLTRB(150.0, 10.0, 2.0, 0.0),

                    //******* separate sender and receiver  messages*******\\
                    padding: isMe == snapshot.value['sender']? const EdgeInsets.fromLTRB(25.0, 10.0, 150.0, 0.0)
                        :const EdgeInsets.fromLTRB(150.0, 10.0, 25.0, 0.0),
                    child: Column(
                      crossAxisAlignment: isMe == snapshot.value['sender']? CrossAxisAlignment.start: CrossAxisAlignment.end,

                    children: <Widget>[

                        new Text(snapshot.value['message'],maxLines: null,//textAlign: TextAlign.center,
                            style:
                            new TextStyle(
                                color: Colors.black,
                                fontSize: 16.0,
                                fontWeight: FontWeight.w900)),
                      ],
                    ),
                  );
                },
              ),
            ),

            // new Text("save ${widget.value2}"),
            new Divider(height: 1.0),

            new Container(
              child: _buildComposer(),

              decoration: new BoxDecoration(color: Theme.of(ctx).cardColor),

            ),
          ],
        )
    );
  }


  Widget _buildComposer() {
    return new IconTheme(
      data: new IconThemeData(color: Theme.of(context).accentColor),
      child: new Container(
        margin: const EdgeInsets.symmetric(horizontal: 9.0),
        child: new Row(

          children: <Widget>[

            new Container(
              margin: new EdgeInsets.fromLTRB(2.0, 0, 2.0, 0),
              child: new IconButton(
                  padding: new EdgeInsets.fromLTRB(0.0, 0, 2.0, 0),
                  icon: new Icon(Icons.camera, size: 30.0,),
                  onPressed: (){
                    print("test");
                    //picker();
                  }// => _handleSubmitted(_textController.text)
              ),
            ),
            new Flexible(
              child: new TextField(
                maxLines: null,
                keyboardType: TextInputType.multiline,
                controller: _textController,
                onChanged: (String txt) {
                  setState(() {
                    _isWriting = txt.length > 0;
                  });
                },
                onSubmitted: _submitMsg,
                decoration: InputDecoration(
                  /*border: OutlineInputBorder(
                           borderRadius: BorderRadius.circular(32.0)
                       )*/
                  //contentPadding: new EdgeInsets.symmetric(vertical: 0.0),
                  // border: InputBorder.none,
                  /* prefixIcon: Padding(
                         padding: EdgeInsets.all(0.0),
                         child: Icon(
                           Icons.camera,
                           color: Colors.grey,
                             //onPressed:(){},
                         ),
                         // icon is 48px widget.
                       ),*/
                  /*suffixIcon:Padding(
                   padding: EdgeInsets.all(0.0),
                   child: Icon(
                     Icons.attachment,
                     color: Colors.grey,
                   ), // icon is 48px widget.
                 ),*/
                    border: InputBorder.none,
                    hintText: "Write a Message"),
              ),

            ),
            new Container(
              // margin: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0),
              child: new IconButton(
                  padding: new EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                  icon: new Icon(Icons.attachment, size: 30.0,),
                  alignment: FractionalOffset.centerLeft,
                  onPressed: (){}// => _handleSubmitted(_textController.text)
              ),
            ),


            new FloatingActionButton(
              //  elevation: 0.0,

                child: new Icon(Icons.near_me),

                backgroundColor: Colors.deepPurpleAccent,

                onPressed: (){_submitMsg(_textController.text);}
              /*_isWriting
                       ? () => _submitMsg(_textController.text)
                       : null,*/
            ),
            /*  new Container(
                   margin: new EdgeInsets.symmetric(horizontal: 3.0),
                   child: Theme.of(context).platform == TargetPlatform.iOS
                       ? new CupertinoButton(
                       child: new Text("Submit"),
                       onPressed: _isWriting ? () => _submitMsg(_textController.text)
                           : null
                   )
                       : new IconButton(
                     icon: new Icon(Icons.send),
                     onPressed: _isWriting
                         ? () => _submitMsg(_textController.text)
                         : null,
                   )
               ),*/
          ],
        ),
        decoration: Theme.of(context).platform == TargetPlatform.iOS
            ? new BoxDecoration(
            border:
            new Border(top: new BorderSide(color: Colors.brown))):null,
      ),
    );
  }

  void _submitMsg(String text) {
    print("working");
    //  uName();
    _sendMessage(sender:isMe, messageText: text, imageUrl: null);

    _textController.clear();
    setState(() {
      _isWriting = false;
    });
  }

  void _sendMessage({ String sender,String messageText, String imageUrl}) {
    print("prinnnnnnnnnn");
    databaseId.push().set({
      'message': messageText,
      'sender' : sender
    });
    databaseId1.push().set({
      'message': messageText,
      'sender' : sender
    });
  }



}
class myData {
  String name, message, profession;

  myData(this.name, this.message, this.profession);
}

class Item {
  String key;
  String sender;
  String message;

  Item(this.sender, this.message);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        sender = snapshot.value["sender"],
        message = snapshot.value["message"];

  toJson() {
    return {
      "sender": sender,
      "message": message,
    };
  }
}


enum HeaderAppearance { arc, profile }

double _getTargetMaxExtent(HeaderAppearance appearance) {
  if (appearance == HeaderAppearance.arc) {
    return 150.0;
  } else {
    return 75.0;
  }
}

double _getTargetArcAnimationValue(HeaderAppearance appearance) {
  if (appearance == HeaderAppearance.arc) {
    return 1.0;
  } else {
    return 0.0;
  }
}

class AnimatedPage extends StatefulWidget {
  AnimatedPage({Key key, this.appearance, this.backgroundImage, this.children}) : super(key: key);

  final HeaderAppearance appearance;
  final String backgroundImage;
  final List<Widget> children;

  @override
  _AnimatedPageState createState() => _AnimatedPageState();
}

class _AnimatedPageState extends State<AnimatedPage> with SingleTickerProviderStateMixin {
  AnimationController _maxExtentAnimation;

  @override
  void initState() {
    super.initState();
    _maxExtentAnimation = AnimationController.unbounded(vsync: this, value: _getTargetMaxExtent(widget.appearance));
  }

  @override
  void didUpdateWidget(AnimatedPage oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.appearance != oldWidget.appearance) {
      _maxExtentAnimation.animateTo(
        _getTargetMaxExtent(widget.appearance),
        duration: Duration(milliseconds: 600),
        curve: Curves.easeInOut,
      );
    }
  }

  @override
  void dispose() {
    _maxExtentAnimation.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _maxExtentAnimation,
      builder: (context, child) {
        return CustomScrollView(
          slivers: <Widget>[
            SliverPersistentHeader(
              pinned: true,
              delegate: AnimatedHeaderDelegate(
                appearance: widget.appearance,
                backgroundImage: widget.backgroundImage,
                minExtent: 50.0,
                maxExtent: _maxExtentAnimation.value,
              ),
            ),
            child,
          ],
        );
      },
      child: SliverList(delegate: SliverChildListDelegate(widget.children)),
    );
  }
}

class AnimatedHeaderDelegate extends SliverPersistentHeaderDelegate {
  AnimatedHeaderDelegate({this.appearance, this.backgroundImage, this.minExtent, this.maxExtent});

  final HeaderAppearance appearance;

  final String backgroundImage;

  @override
  final double minExtent;
  @override
  final double maxExtent;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final shrinkRelative = shrinkOffset / (maxExtent - minExtent);
    return AnimatedHeader(
      appearance: appearance,
      backgroundImage: backgroundImage,
      curvatureMultiplier: 1.0 - shrinkRelative,
    );
  }

  @override
  bool shouldRebuild(AnimatedHeaderDelegate oldDelegate) {
    return appearance != oldDelegate.appearance ||
        minExtent != oldDelegate.minExtent ||
        maxExtent != oldDelegate.maxExtent;
  }
}

class AnimatedHeader extends StatefulWidget {
  AnimatedHeader({Key key, this.appearance, this.backgroundImage, this.curvatureMultiplier}) : super(key: key);

  final HeaderAppearance appearance;

  final String backgroundImage;

  final double curvatureMultiplier;

  @override
  _AnimatedHeaderState createState() => _AnimatedHeaderState();
}

class _AnimatedHeaderState extends State<AnimatedHeader> with TickerProviderStateMixin {
  AnimationController _arcAnimation;

  @override
  void initState() {
    super.initState();
    _arcAnimation = AnimationController(
      vsync: this,
      value: _getTargetArcAnimationValue(widget.appearance),
      duration: Duration(milliseconds: 600),
    );
  }

  @override
  void didUpdateWidget(AnimatedHeader oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.appearance != oldWidget.appearance) {
      _arcAnimation.animateTo(_getTargetArcAnimationValue(widget.appearance));
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(

      animation: CurvedAnimation(parent: _arcAnimation, curve: Curves.linear),
      builder: (context, child) {
        return ClipPath(
          clipper: ArcClipper(
            curvature: _arcAnimation.value * widget.curvatureMultiplier,
          ),
          clipBehavior: Clip.antiAlias,
          child: child,
        );
      },
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          AnimatedSwitcher(
            duration: Duration(milliseconds: 600),
            child: Container(
              key: ValueKey(widget.backgroundImage),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(widget.backgroundImage),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Center(
            child: Text(
              'TITLE',
              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w500),
            ),
          ),
        ],
      ),
    );
  }
}

class ArcClipper extends CustomClipper<Path> {
  ArcClipper({this.curvature});

  final double curvature;

  @override
  Path getClip(Size size) {
    if (curvature == 0.0) {
      return Path()..addRect(Offset.zero & size);
    } else {
      return Path()
        ..moveTo(0.0, 0.0)
        ..lineTo(size.width, 0.0)
        ..lineTo(size.width, size.height)
        ..quadraticBezierTo(size.width / 2, size.height - size.height * 0.4 * curvature, 0.0, size.height)
        ..close();
    }
  }

  @override
  bool shouldReclip(ArcClipper oldClipper) {
    return curvature != oldClipper.curvature;
  }
}