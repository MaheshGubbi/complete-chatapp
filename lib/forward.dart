
import 'package:firebase/home_page.dart';
import 'package:firebase/messages.dart';
import 'package:firebase/msg.dart';
import 'package:firebase/msg1.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';


class Forward extends StatefulWidget {
  // Group(TextEditingController grpController);
  final String message;
  final String isMe;
  final String tomsg;
  Forward( {Key key, this.isMe, this.message, this.tomsg,}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Forward> {
  _getRequests()async{

  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String userId;
  String OwnerName;
  String groupName;
  String isMe;
  String message;

  List _selecteCategorys = List();
  List<Item> UsersList =new  List();
  Item item;
  DatabaseReference databaseMsg;
  DatabaseReference itemRef;
  DatabaseReference itemRefGrp;
  DatabaseReference databaseId;
  DatabaseReference databaseIdG;
  DatabaseReference databaseIdM;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _init();
    uName();
    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance;
    //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    itemRefGrp = database.reference().child('messagalu');
    itemRef.onChildAdded.listen(_onEntryAdded);
  }

  Future _init() async {
    await FirebaseDatabase.instance.setPersistenceEnabled(true);
    isMe = "${widget.isMe}";
    message = "${widget.message}";
    databaseMsg = FirebaseDatabase.instance.reference().child("messagalu").child("$isMe");

  }

  void uName() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    userId = user.uid;
    Object _objdatabase;
    // single value retrieve
    await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
        DataSnapshot snapshot) {
      _objdatabase = snapshot.value;
      OwnerName = _objdatabase;
      print("owner : $OwnerName");
    });
    //return OwnerName;
  }

  _onEntryAdded(Event event) {
    setState(() {
      UsersList.add(Item.fromSnapshot(event.snapshot));
    });
  }

  void _onCategorySelected(bool selected, index) {
    if (selected == true) {
      setState(() {
        _selecteCategorys.add(index);

      });
    } else {
      setState(() {
        _selecteCategorys.remove(index);

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Forward to...'),
        backgroundColor:Colors.deepPurpleAccent,
        actions: <Widget>[

          PopupMenuButton<String>(
            onSelected: (String value){},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Search'))

              ];
            },
          ),
        ],
      ),
//      floatingActionButton: FloatingActionButton(
//        child: const Icon(Icons.add), onPressed: () {},);
      floatingActionButton: FloatingActionButton(
        elevation: 4.0,
        backgroundColor: Colors.deepPurpleAccent,
        child: const Icon(Icons.send),
       // label: const Text('Send'),
        onPressed: () {
          // creating groups in personal accounts
          for (final x in _selecteCategorys) {
            //addP(x);
            forwardMsg(x);
            print("$x");
          }
          // addG();

          //_showDialog();
          Navigator.of(context).pushReplacement(
            new MaterialPageRoute(builder: (_)
            =>new messageHere(value2: "${widget.isMe}", value1: "${widget.tomsg}")),)
              .then((val)=>val?_getRequests():null);
        },
      ),
      //resizeToAvoidBottomPadding: true,
      body:
      // Container(child: new Text("text"))
      Column(
        children: <Widget>[
          //new Text("Select to Add contact "),
          new Flexible(

              child:  FirebaseAnimatedList(
                query: itemRef,
                /* sort: _anchorToBottom
                    ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                    : null,*/
                // itemCount: UsersList.length,
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                sort: (a, b) => b.key.compareTo(a.key),

                itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation,int index) {
                  return new CheckboxListTile(
                    //// sizeFactor: animation,
                    // new Icon(Icons.person),
                    title: Text(UsersList[index].name, style: TextStyle(fontSize: 18.0),),
                    value: _selecteCategorys
                        .contains(UsersList[index].name),
                    onChanged: (bool selected) {
                      _onCategorySelected(selected,
                          UsersList[index].name);
                    },
                  );
                },
              )
          ),
        ],

      ),
    );
  }



  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
  }


//****** forward message all selected person*******\\
  void forwardMsg(x) {
    databaseMsg.child("$x").push().set({
      'sender': '$isMe',
      'message': '$message',
      //'Type' : 'Group'
    });
  }





}
//****** List of item *******\\
class Item {
  String key;
  String name;
  String Number;


  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["Number"];

  toJson() {
    return {
      "name": name,
      "Number": Number,
    };
  }
}



