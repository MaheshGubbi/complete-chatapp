import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

abstract class BaseAuth {
  Future<String> signInWithEmailAndPassword(String email, String password);
  Future<String> createUserWithEmailAndPassword(String email, String password);
  Future<String> currentUser();
  Future<String> currentName();
  Future<void> signOut();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  //******* login with Email and Password *******\\
  Future<String> signInWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    user.email;
    return user?.uid;
  }

  //******* Registered Email and password *******\\
  Future<String> createUserWithEmailAndPassword(
      String email, String password) async {
    FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    return user?.uid;
  }

  //******* Getting User Id *******\\
  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user?.uid;
  }

  //******* Getting User user Name*******\\
  Future<String> currentName() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
   String userId = user.uid;
    Object _objdatabase;
    // single value retrieve
    await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
        DataSnapshot snapshot) {
      _objdatabase = snapshot.value;
     String OwnerName = _objdatabase;
      print("working : $OwnerName");
    //  itemRefM = FirebaseDatabase.instance.reference().child('messagalu').child('$OwnerName');

    });
    return _objdatabase;
  }

  //******* signout *******\\
  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }
}
