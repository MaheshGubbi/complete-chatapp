
import 'package:firebase/home_page.dart';
import 'package:firebase/messages.dart';
import 'package:firebase/msg.dart';
import 'package:firebase/msg1.dart';
import 'package:firebase/msgGroup.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';


class Group extends StatefulWidget {
 // Group(TextEditingController grpController);
  final String grpName;
  final String isMe;
  Group( {Key key, this.grpName, this.isMe}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Group> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String userId;
  String OwnerName;
  String groupName;
  String isMe;

  List _selecteCategorys = List();
  List<Item> UsersList =new  List();
  Item item;
  DatabaseReference itemRef;
  DatabaseReference itemRefGrp;
  DatabaseReference databaseId;
  DatabaseReference databaseIdG;
  DatabaseReference databaseIdM;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();

  @override
  void initState() {
    super.initState();
    _init();
    uName();
    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance;
    //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    itemRefGrp = database.reference().child('messagalu');
    itemRef.onChildAdded.listen(_onEntryAdded);
  }

  Future _init() async {
    await FirebaseDatabase.instance.setPersistenceEnabled(true);
    groupName = "${widget.grpName}";
    isMe = "${widget.isMe}";
    print("gggggggggggg $groupName");
    print("meeeeeeeeeee $isMe");
  }

  void uName() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    userId = user.uid;
    Object _objdatabase;
    // single value retrieve
    await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
        DataSnapshot snapshot) {
      _objdatabase = snapshot.value;
      OwnerName = _objdatabase;
      print("owner : $OwnerName");
    });
    //return OwnerName;
  }

  _onEntryAdded(Event event) {
    setState(() {
      UsersList.add(Item.fromSnapshot(event.snapshot));
    });
  }

  void _onCategorySelected(bool selected, index) {
    if (selected == true) {
      setState(() {
        print("sssss $selected");
        print("nnnnn $UsersList[index].name");
        _selecteCategorys.add(index);
        print("ppppppp $_selecteCategorys");
        print("$index");
        //add(index);
      //  print("dddddadsadsd $grpController");
        //String pass = "_${UsersList[index].name}";
        //itemRefGrp.child('1');

      });
    } else {
      setState(() {
        print("sssss $selected");
        print("$UsersList[index].name");
        _selecteCategorys.remove(index);
        print("$index");
        //deleteData(index);

        //String pass = "_${UsersList[index].name}";
        //itemRefGrp.child('1');//.child('$pass').remove();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Create Group'),
        backgroundColor:Colors.deepPurpleAccent,
        actions: <Widget>[

          PopupMenuButton<String>(
            onSelected: (String value){},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'))

              ];
            },
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        elevation: 4.0,
        backgroundColor: Colors.deepPurpleAccent,
        icon: const Icon(Icons.add),
        label: const Text('Save a Group'),
        onPressed: () {
         // creating groups in personal accounts
          for (final x in _selecteCategorys) {
            addP(x);
            //addM(x);
            print("$x");
          }
          addG();
          
          //_showDialog();
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(

              builder: (BuildContext context) => new messageGroup(value2: isMe, value1: groupName),
            ),
          );
        },
      ),
      //resizeToAvoidBottomPadding: true,
      body:
      // Container(child: new Text("text"))
      Column(
        children: <Widget>[
           new Text("Select to Add contact "),
          new Flexible(

              child:  FirebaseAnimatedList(
                query: itemRef,
                /* sort: _anchorToBottom
                    ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                    : null,*/
                // itemCount: UsersList.length,
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                sort: (a, b) => b.key.compareTo(a.key),

                itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation,int index) {
                  return new CheckboxListTile(
                    //// sizeFactor: animation,
                   // new Icon(Icons.person),
                    title: Text(UsersList[index].name, style: TextStyle(fontSize: 18.0),),

                    value: _selecteCategorys
                        .contains(UsersList[index].name),
                    onChanged: (bool selected) {
                      _onCategorySelected(selected,
                          UsersList[index].name);
                     // print(UsersList[index].name);
                    },
                    // itemCount: UsersList.length,  snapshot.value.toString()

                 /*   onTap: () { },
                    onLongPress: (){
//                      for (UsersList[index].name p in widget.UsersList) {
//                        if (p.isCheck)
//                          print(p.name);
//                      }
                      //  itemRef.onChildRemoved.listen(_onEntryRemoved);
                    },*/
                  );
                  print("sssssss $_selecteCategorys[1]");
                },

              )
          ),
          //(onPressed: null,child: ,),



      ],

      ),
    );
  }



  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
  }

  void
  _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Create A Group",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: new TextField(style: TextStyle(fontSize: 20.0,color: Colors.black,
              fontStyle: FontStyle.normal,fontWeight: FontWeight.bold),
            maxLength: 20,textAlign: TextAlign.start,decoration: InputDecoration
              (labelText: "Enter Group name", ),), //***labelStyle:
          actions: <Widget>[
            new FlatButton(
              child: new Text("Save",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => new Messages(),
                  ),
                );
                //Navigator.of(context).pop();
              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cancel",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void addP(index) {

    itemRefGrp.child("$index").child("$groupName").push().set({
      'sender': '$isMe',
      'message': 'hi.. welcome to "$groupName" Group',
      'Type' : 'Group'
    });
    databaseIdM = FirebaseDatabase.instance.reference().child("GroupMessage");
    databaseIdM.child("$groupName").child("Mems").push().set({
      'Members': '$index',
    });
  }
  void deleteData(index){
    itemRefGrp.child("$index").child("$groupName").remove();
  }

  void addG() {
    databaseIdG = FirebaseDatabase.instance.reference().child("GroupMessage");
    databaseIdG.child("$groupName").child("Messages").push().set({
      'sender': '$isMe',
      'message': 'hi.. welcome to "$groupName" Group',
      'Type' : 'Group'
    });

  }

 /* void addM(x) {
    databaseIdM = FirebaseDatabase.instance.reference().child("GroupMessage");
    databaseIdM.child("$groupName").child("Mems").push().set({
      'Members': '$x',
    });
    print("mems $x");
  }*/




}
//****** List of item *******\\
class Item {
  String key;
  String name;
  String Number;


  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["Number"];

  toJson() {
    return {
      "name": name,
      "Number": Number,
    };
  }
}



