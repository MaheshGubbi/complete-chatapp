
import 'package:firebase/PrnProfile.dart';
import 'package:firebase/home_page.dart';
import 'package:firebase/msg.dart';
import 'package:firebase/profile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';



class Contact extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Contact> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String userId;
  String OwnerName;


  List<Item> UsersList =new  List();
  Item item;
  DatabaseReference itemRef;
  DatabaseReference databaseId;

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController controller = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();

  @override
    void initState() {
    super.initState();
    _init();
    uName();
    item = Item("", "");
    final FirebaseDatabase database = FirebaseDatabase.instance;
    //Rather then just writing FirebaseDatabase(), get the instance.
    itemRef = database.reference().child('User');
    itemRef.onChildAdded.listen(_onEntryAdded);
    }

  Future _init() async {
    await FirebaseDatabase.instance.setPersistenceEnabled(true);

  }

  void uName() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    userId = user.uid;
    Object _objdatabase;
    // single value retrieve
    await FirebaseDatabase.instance.reference().child("account").child("$userId").child("details").child("name").once().then((
        DataSnapshot snapshot) {
      _objdatabase = snapshot.value;
      OwnerName = _objdatabase;
      print("owner : $OwnerName");
      });
    //return OwnerName;
  }

  _onEntryAdded(Event event) {
    setState(() {
      UsersList.add(Item.fromSnapshot(event.snapshot));
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Contacts List'),
        backgroundColor:Colors.deepPurpleAccent,
        actions: <Widget>[

          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => new HomePage(),
                ),
              );
            },
          ),

          PopupMenuButton<String>(
            onSelected: (String value){},
            itemBuilder: (BuildContext context) {
              return [
                PopupMenuItem(child: Text('Settings')),
                PopupMenuItem(child: Text('Logout'))

              ];
            },
          ),
        ],
      ),
      //resizeToAvoidBottomPadding: true,
      body:
     // Container(child: new Text("text"))
       Column(
         children: <Widget>[
          // new Text("contact list"),
           new Flexible(

              child:  FirebaseAnimatedList(
              query: itemRef,
               /* sort: _anchorToBottom
                    ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                    : null,*/
               // itemCount: UsersList.length,
                padding: new EdgeInsets.all(8.0),
                reverse: false,
                sort: (a, b) => b.key.compareTo(a.key),

                itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation,int index) {
              return new ListTile(
               //// sizeFactor: animation,
                //leading: Icon(Icons.person),
                leading:  GestureDetector(
                  child: new CircleAvatar(
                    child: new Text(UsersList[index].name.toString().toUpperCase().substring(0, 2)),
                  ),
                  onTap: (){

                    _showDialog(UsersList[index].name);
                    //print(snapshot.value['name']);
                  } /*(){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        //builder: (BuildContext context) => new profile(group :"$UsersList[index].name}"),
                      ),
                    );
                  },*/
                ),

                title: GestureDetector(
                  child: new Text(UsersList[index].name, style: TextStyle(fontSize: 18.0),),
               // itemCount: UsersList.length,  snapshot.value.toString()

                onTap: () {
                  final toMessage = UsersList[index].name;
                 // final dash = "_${UsersList[index].name}";
                  final isme = "$OwnerName";
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new messageHere(value2: isme, value1: toMessage),
                    ),
                  );
                },
                onLongPress: (){
                //  itemRef.onChildRemoved.listen(_onEntryRemoved);
                },
                ),
              );
            },

              )
           ),

         ],

       ),
    );
  }



  void nameId() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String userId = user.uid;
    databaseId = FirebaseDatabase.instance.reference().child("messagalu").child("$userId");
    }

  void _showDialog(value) {
    // flutter defined function
    //https://goodbusinesses.info/wp-content/uploads/2018/11/Personal-Bank-Loan-ME-Bank.jpg
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
         // title: new Text("$value",style: TextStyle(fontSize: 24.0,color: Colors.purpleAccent),),
          content: new Image.network(//Image.asset("images/one.jpg",
          "https://rukminim1.flixcart.com/image/704/704/poster/p/n/k/amy-boys-its-me-amyps10000486-original-imae4npemq25jryz.jpeg?q=70",
        //  "https://goodbusinesses.info/wp-content/uploads/2018/11/Personal-Bank-Loan-ME-Bank.jpg",
          //"https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
          fit: BoxFit.cover,

        ), //***labelStyle:
          actions:
          <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(50.0, 0.0, 50.0, 0.0),
                      child: //new Icon(Icons.message),
                      new IconButton(icon: Icon(Icons.message),
                          onPressed: (){
                            Navigator.of(context).pop();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (BuildContext context) =>new messageHere(value2: OwnerName, value1: value),
                              ),
                            );
                          }

                      )

                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(30.0, 0.0, 50.0, 0.0),
                      child: new IconButton(icon: Icon(Icons.info),
                          onPressed: (){
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context)=> new PrnProfile(isMe:value)
                            ),
                              );
                          }
                      ) ,
                    ),
                  ],
                ),
              ),
            ),
           /* new FlatButton(
              child: new Text("Save",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),

              onPressed: () {

              },
            ),
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cancel",style: TextStyle(color: Colors.deepPurpleAccent,fontSize: 16.0),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),*/
          ],
        );
      },
    );
  }

  void msg(value) {
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (BuildContext context) =>new messageHere(value2: OwnerName, value1: value),
      ),
    );
  }
}
//****** List of item *******\\
class Item {
  String key;
  String name;
  String Number;


  Item(this.name, this.Number);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        name = snapshot.value["name"],
        Number = snapshot.value["Number"];

  toJson() {
    return {
      "name": name,
      "Number": Number,
    };
  }
}



