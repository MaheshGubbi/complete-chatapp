
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class profile extends StatefulWidget {
  final String group;

  profile({Key key, this.group,}) : super(key: key);

  @override
  _profileState createState() => _profileState();
}

class _profileState extends State<profile> {
  String GName;
  int index =20;
  DatabaseReference itemRef;
  DatabaseReference itemRefMem;

  bool selected = false;
  var SelectedState = false;
  var mycolor=Colors.white;
  var isSelected = false;

  List<Item> UsersList = List();
  Item item;


  @override
  void initState() {
    super.initState();
    item = Item("", "");
    GName = "${widget.group}";
    final FirebaseDatabase database = FirebaseDatabase.instance;
    itemRefMem = database.reference().child("GroupMessage").child(GName).child("Mems");

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Padding(
                    padding: const EdgeInsets.fromLTRB(50.0, 0, 0, 0),
                    child: Text("$GName",textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0, fontWeight: FontWeight.bold,

                        )),
                  ),


                    background: Image.network(//Image.asset("images/one.jpg",
                      "https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/06/09/11/group-photos-need-to-die.jpg?auto=compress&cs=tinysrgb&h=350",
                      //"https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350",
                      fit: BoxFit.cover,
                    )),
            ),
           // Text("Group Members",style: TextStyle(fontSize: 25.0,color: Colors.deepPurpleAccent),),

          ];
        },
        body: Column(
          children: <Widget>[
          //  new Divider(height: 10.1),
//          Center( child: new Text(" sssssssssss $OwnerName")),
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
              child: new Text("$GName Group Members list",style: TextStyle(fontSize: 25.0,color: Colors.deepPurple,fontWeight: FontWeight.bold),),
            ),
            new Flexible(

                child:  FirebaseAnimatedList(
                  query: itemRefMem,
                  /* sort: _anchorToBottom
                    ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
                    : null,*/
                   //itemCount: UsersList.length,
                  padding: new EdgeInsets.fromLTRB(10, 10.0, 10.0, 0.0),
                  reverse: false,
                  // sort: (a, b) => b.key.compareTo(a.key),
//                sort: (DataSnapshot a, DataSnapshot b) =>
//                    b.key.compareTo(a.key),
                 // sort: (a, b) => b.value['message'].toString().compareTo(a.value['message'].toString()),
                  itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation,int index) {
                    return Container(
                    /*  decoration: new BoxDecoration(boxShadow: [
                          new BoxShadow(
                            color: Colors.black12,
                            blurRadius: 0.0,
                          ),
                      ]),*/
                      child: Column(mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
//                          GestureDetector(
//                            child: new CircleAvatar(
//                              child: new Text(snapshot.value['Members'].toString().substring(0, 1)),
//                            ),
//                            onTap: null,
//                          ),
                          new  ListTile(
                          leading:  GestureDetector(
                          child: new CircleAvatar(
                          child: new Text(snapshot.value['name'].toString().substring(0, 1)),
                            ),
                          ),
                            selected: isSelected,
                            //// sizeFactor: animation,
                           /* leading: new CircleAvatar(
                              child: new Text(snapshot.value['Members'].toString().substring(0, 1)),
                            ),*/

                            /*leading:
                         new CircleAvatar(
                           child:  new Image.network("http://res.cloudinary.com/kennyy/image/upload/v1531317427/avatar_z1rc6f.png"),
                         ),*/

                            //Icon(Icons.person),
                            title: Text(snapshot.value['Members']),
                            //subtitle: Text(msgList[index].toString(), style: TextStyle(fontSize: 18.0),),
                            //trailing: const Icon(Icons.arrow_forward_ios),
                            onTap: () {

                            },
                            onLongPress: (){
                              setState(() {
                                mycolor=Colors.red;});//_showDialog();

//
                            },
                          ),
                        ],
                      ),

                    );
                  },
                )
            ),
          ],

        ),
      ),
    );
  }
}

class Item {
  String key;
  String Members;
  String message;

  Item(this.Members, this.message);

  Item.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        Members = snapshot.value["Members"],
        message = snapshot.value["message"];

  toJson() {
    return {
      "Members": Members,
      "message": message,
    };
  }
}